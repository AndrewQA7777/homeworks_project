package dictionaries;

public class MessageText {

    public static final String SEPARATOR = "====================";

    public static final String TIMEOUT_ERROR = "Error: timeout reached";

    public static final String FAILED_CLICK = "Warn: there was unsuccess click!";
    public static final String FAILED_LOGOUT = "Logout failed!";
    public static final String FAILED_SAVE = "Save request was failed!";

    public static final String SUCCEED_CLICK = "Click by element was succeed";
    public static final String SUCCEED_CLICK_SECOND_ATTEMPT = "Click by element was succeed, but at the second attempt";
    public static final String SUCCEED_SENDKEYS = "Sendkeys to element was succeed";
    public static final String NO_SUCCESS_MESSAGE = "Success message wasn't displayed!!!";
    public static final String NO_EXPECTED_MESSAGE = "Expected message wasn't displayed!!!";
    public static final String NO_EXPECTED_TEXT = "Expected text wasn't displayed!!!";
    public static final String ACTIVE_PAGE_IS_NOT_HIGHLIGHTED = "Active page is not highlighted in pagination!!!";
    public static final String PROBLEM_WITH_TABS = "There is missing tab or default tab is wrong";
}