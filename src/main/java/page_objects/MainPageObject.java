package page_objects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Step;

public class MainPageObject extends BasePageObject{

    By searchFieldLocator = By.xpath("//div[contains(@class,'center-block')]//input");
    By searchIconLocator = By.xpath("//div[contains(@class,'center-block')]//a[@data-ng-click='sendSearch($event)']");
    By linkInSnippetLocator = By.xpath("//div[contains(@class,'snippet')]//a[@class='ng-binding']");
    By allResultsTabChosenLocator = By.xpath("//li[contains(@class,'active') and contains(.,'Все')]");
    By moreResultsButtonLocator = By.xpath("//a[@ng-click='showAdded();']");

    public MainPageObject(WebDriver driver) {
        super(driver);
    }

    @Step("Производим поиск с запросом '{0}'")
    public void performSearch(String query){
        driverHelper.clear(searchFieldLocator);
        driverHelper.sendKeys(searchFieldLocator, query);
        driverHelper.click(searchIconLocator);
    }

    @Step("Производим проверки элементов в результатах после осуществления поиска")
    public boolean checkSearchResultsElements(){
        boolean moreResultsButtonWorksFine;
        if(driverHelper.checkElementVisibility(By.xpath("//div[@ng-repeat='result in results track by $index'][19]"))){
            moreResultsButtonWorksFine = driverHelper.checkElementClickability(moreResultsButtonLocator);
        } else {
            moreResultsButtonWorksFine = !driverHelper.checkElementVisibility(moreResultsButtonLocator);
        }
        return
            driverHelper.checkElementClickability(linkInSnippetLocator) && // на локатор есть два элемента - проверяем наличие минимум одного
            driverHelper.checkElementVisibility(allResultsTabChosenLocator) &&
            !driverHelper.checkElementVisibility(By.xpath("//div[@ng-repeat='result in results track by $index'][20]")) &&
            moreResultsButtonWorksFine;
    }
}
