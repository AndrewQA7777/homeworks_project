package page_objects;

import helpers.DriverHelper;
import helpers.Waiters;
import org.openqa.selenium.WebDriver;

public abstract class BasePageObject {

    protected WebDriver driver;
    protected DriverHelper driverHelper;
    protected Waiters waiters;

    public BasePageObject(WebDriver driver) {
        this.driver = driver;
        this.driverHelper = new DriverHelper(driver);
        this.waiters = new Waiters(driver);
    }
}
