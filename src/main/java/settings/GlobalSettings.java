package settings;

import dictionaries.Environments;
import helpers.CreateDriver;

public class GlobalSettings {

    //Таймаут для ждаторов
    public static final int GLOBAL_TIMEOUT = 30;
    //Стандартный минимальный таймаут проекта
    public static final int DEFAULT_TIMEOUT = 15;
    //Стандартный максимал таймаут проекта



    public static final int PRELOAD_TIMEOUT = 10;


    //Тип используемого драйвера
    public static final String DRIVER = CreateDriver.CHROME;
    public static final String REMOTE = CreateDriver.LOCAL;

    //Окружение для запуска тестов
    public static final String URL = Environments.PRODUCTION;
}
