package settings;

import dictionaries.Environments;
import helpers.CreateDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class SettingsParser {

    public void getSettings() {

        try {
            Properties props = new Properties();
            FileInputStream fis = new FileInputStream("src/main/java/settings/settings.xml");

            props.loadFromXML(fis);

            int GLOBAL_TIMEOUT = Integer.parseInt(props.getProperty("GLOBAL_TIMEOUT"));

            int IMPLICITLY_WAIT = Integer.parseInt(props.getProperty("IMPLICITLY_WAIT"));

            int DEFAULT_TIMEOUT = Integer.parseInt(props.getProperty("DEFAULT_TIMEOUT"));

            int PRELOAD_TIMEOUT = Integer.parseInt(props.getProperty("PRELOAD_TIMEOUT"));

   /*         String DRIVER;
            switch(props.getProperty("DRIVER")) {
                case "CHROME":
                    DRIVER = CreateDriver.CHROME;
                    break;
                case "IE":
                    DRIVER = CreateDriver.IE;
                    break;
                case "FIREFOX":
                    DRIVER = CreateDriver.FIREFOX;
                    break;
                case "OPERA":
                    DRIVER = CreateDriver.OPERA;
                    break;
            }

            String REMOTE;
            switch(props.getProperty("REMOTE")) {
                case "LOCAL":
                    DRIVER = CreateDriver.LOCAL;
                    break;
                case "REMOTE":
                    DRIVER = CreateDriver.REMOTE;
                    break;
            }

            String URL;
            switch(props.getProperty("URL")) {
                case "LOCAL":
                    DRIVER = CreateDriver.LOCAL;
                    break;
                case "REMOTE":
                    DRIVER = CreateDriver.REMOTE;
                    break;
            }

            String AUTH;
            switch(props.getProperty("AUTH")) {
                case "BASE_AUTH":
                    AUTH = Environments.BASE_AUTH;
                    break;
                case "REMOTE":
                    AUTH = Environments.EMPTY_AUTH;
                    break;
            }

            String DB_URL;
            switch(props.getProperty("DB_URL")){
                case "DBAUTOTEST01":
                    DB_URL = Environments.DBAUTOTEST01;
            }

            Users USERS;
            switch(props.getProperty("USERS")){
                case "MINTA":
                    USERS = Users.MINTA;
                case "DEV":
                    USERS = Users.DEV;
                case "DEV2":
                    USERS = Users.DEV2;
            }*/

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
