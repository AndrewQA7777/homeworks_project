package helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Random;

public class ValueGenerator {

    private Random randomGenerator = new Random();

    public String generateRandomText(int length) {

        String possible = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫБЭЮЯабвгдежзийкламнопрстуфхцчшщъыьэюя";
        String text = "ТЕСТ_";

        for (int i = 0; i < length; i++) {
            text += possible.charAt(randomGenerator.nextInt(possible.length()));
        }

        return text;
    }

    public String generateRandomNumber(int length) {

        String possible = "0123456789";
        String text = "";

        for (int i = 0; i < length; i++) {
            text += possible.charAt(randomGenerator.nextInt(possible.length()));
        }

        return text;
    }

    public int randomInt(int size){
        return randomGenerator.nextInt(size);
    }

    public int randomIndex(int size){
        if (size == 1) {
            return 0;
        } else {
            return randomGenerator.nextInt(size);
        }
    }

    public String getCurrentDate(){
        return LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }

    public String getCurrentDatePlusDays(int days){
        return LocalDate.now().plusDays(days).format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }
}
