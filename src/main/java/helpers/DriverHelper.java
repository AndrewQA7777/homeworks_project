package helpers;

import core.Singleton;
import dictionaries.MessageText;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;
import settings.GlobalSettings;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.lang.*;
import java.util.List;

public class DriverHelper {

    public DriverHelper(WebDriver driver){
        this.driver = driver;
        this.waiters = new Waiters(driver);
        this.valueGenerator = new ValueGenerator();
    }

    private WebDriver driver;
    private Waiters waiters;
    private ValueGenerator valueGenerator;

    public void click(By locator) {
        WebElement myDynamicElement = waiters.waitForClickabilityOfElement(locator);
        int i = 0;
        while (i < 20) {
            try {
                myDynamicElement.click();
                return;
             }
            catch (WebDriverException e){
                i++;
                waiters.timeout(1);
                continue;
                }
            }
    }


    public String stringNormalizer(String inputString) {
        while (inputString.contains(" ")){
            String buffer = inputString.replace(" ", "");
            inputString = buffer;
        }
        while (inputString.contains("\u00AD")){
            String buffer = inputString.replace("\u00AD", "");
            inputString = buffer;
        }
        return inputString;
    }

    public void clickByJavaScript(By locator) {
        WebElement element = driver.findElement(locator);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", element);
    }

    public void clickByJavaScript(WebElement element) {
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", element);
    }

    public void sendKeys(By locator, String keys){
        WebElement myDynamicElement = (new WebDriverWait(driver, 15))
                .until(ExpectedConditions.presenceOfElementLocated(locator));
        myDynamicElement.sendKeys(keys);
    }

    public void clear(By locator){
        WebElement myDynamicElement = (new WebDriverWait(driver, 15))
                .until(ExpectedConditions.presenceOfElementLocated(locator));
        myDynamicElement.clear();
    }

    public boolean checkElementVisibility(By locator) {
        try {
            WebElement element = (new WebDriverWait(driver, 10))
                    .until(ExpectedConditions.visibilityOfElementLocated(locator));
            return element.isDisplayed();
        } catch (TimeoutException te) {
            return false;
        }
    }

    public boolean checkElementClickability(By locator) {
        try {
            WebElement element = (new WebDriverWait(driver, 15))
                    .until(ExpectedConditions.elementToBeClickable(locator));
            return element.isDisplayed();
        } catch (TimeoutException te) {
            return false;
        }
    }

    public boolean checkElementVisibility(WebElement element) {
        int i = 0;

        while (i < GlobalSettings.PRELOAD_TIMEOUT) {
            i++;
            waiters.timeout(1);
            try {
                element.isDisplayed();
                return true;
            } catch (StaleElementReferenceException se) {
                continue;
            } catch (ElementNotVisibleException ee) {
                continue;
            } catch (NoSuchElementException ne) {
                continue;
            }
        }
        return false;
    }

    public boolean checkElementToEdit(WebElement element){
        try {
            element.clear();
            return true;
        }catch (InvalidElementStateException ex){
            return false;
        }
    }

    public boolean checkElementEnablind (By locator){
        WebElement element = waiters.waitForPresenceOfElement(locator);
        try {
            return element.getAttribute("disabled").isEmpty();
        }catch (NullPointerException ex){
            return true;
        }
    }

    public void waitForAjaxRequests(WebDriver driver) {
      try {


          WebDriverWait wait = new WebDriverWait(driver, Long.parseLong("10000"));
          wait.until(new ExpectedCondition<Boolean>() {
              public Boolean apply(org.openqa.selenium.WebDriver driver) {
                  return (Boolean) ((JavascriptExecutor) driver).executeScript("return jQuery.active == 0");
              }
          });
      }catch (WebDriverException ex){

      }
    }

    public void attachFile(By locator){
        File testFile = new File("src/main/resources/test_file.xlsx");
        String absolutePath = testFile.getAbsolutePath();
        WebElement myDynamicElement = (new WebDriverWait(driver, 20))
                .until(ExpectedConditions.presenceOfElementLocated(locator));
        sendKeys(locator, absolutePath);
        click(By.xpath("//a[@ng-click='startUpload()']"));
        waiters.waitForVisibilityOfElement(By.xpath("//div[contains(text(), 'Файл загружен')]"));
    }

    public void takeScreenshot(WebDriver driver) throws IOException {

        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        Calendar cal = Calendar.getInstance();
        String time = cal.getTime().toString().replace(":", "-");
        FileUtils.copyFile(scrFile, new File("./output/Failure_" + time + ".jpg"));
    }

    @Step("Сменить активную вкладку")
    public void changeTab(){
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(0));
        driver.close();
        driver.switchTo().window(tabs.get(1));
    }

    public void openLinkAtNewTab(By locator){

        Actions as=new Actions(driver);
        as.moveToElement(driver.findElement(locator)).keyDown(Keys.CONTROL).click().perform();
    }

    public void openLinkAtNewTab(WebElement element){

        Actions as=new Actions(driver);
        as.moveToElement(element).keyDown(Keys.CONTROL).click().perform();
    }


}
