package helpers;

import core.Singleton;
import dictionaries.Environments;
import io.github.bonigarcia.wdm.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.MarionetteDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import settings.GlobalSettings;

import java.net.MalformedURLException;
import java.net.URL;

public class CreateDriver {

    public final static String FIREFOX = "firefox";
    public final static String CHROME = "chrome";
    public final static String IE = "ie";
    public final static String OPERA = "opera";
    public final static String EDGE = "edge";
    public final static String PHANTOMJS = "phantomjs";
    public final static String MARIONETTE = "marionette";

    public final static String LOCAL = "local";
    public final static String REMOTE = "remote";

    public WebDriver createDriver(String type){

        if(GlobalSettings.REMOTE.equals(LOCAL)){
            return createLocalDriver(type);
        } else {
            Singleton.LOGGER.error("Ошибка при вызове драйвера!");
        }

        return null;
    }

    private WebDriver createLocalDriver(String type) {

        if (type.equals(CreateDriver.FIREFOX)) {
            return createFireFoxDriver();
        }
        if (type.equals(CreateDriver.CHROME)) {
            return createChromeDriver();
        }
        if (type.equals(CreateDriver.IE)) {
            return createInternetExplorerDriver();
        }
        if (type.equals(CreateDriver.OPERA)) {
            return createOperaDriver();
        }
        if (type.equals(CreateDriver.EDGE)) {
            return createEdgeDriver();
        }
        if (type.equals(CreateDriver.PHANTOMJS)) {
            return createPhantomJSDriver();
        }
        if (type.equals(CreateDriver.MARIONETTE)) {
            return createMarionetteDriver();
        }

        return new ChromeDriver();
    }

    private WebDriver createChromeDriver() {
        ChromeDriverManager.getInstance().setup();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");

        return new ChromeDriver(options);
    }

    private WebDriver createFireFoxDriver() {
        return new FirefoxDriver();
    }

    private WebDriver createInternetExplorerDriver() {
        InternetExplorerDriverManager.getInstance().setup();

        return new InternetExplorerDriver();
    }

    private WebDriver createOperaDriver() {
        OperaDriverManager.getInstance().setup();

        return new OperaDriver();
    }

    private WebDriver createEdgeDriver() {
        EdgeDriverManager.getInstance().setup();

        return new EdgeDriver();
    }

    private WebDriver createPhantomJSDriver() {
        PhantomJsDriverManager.getInstance().setup();

        return new PhantomJSDriver();
    }

    private WebDriver createMarionetteDriver() {
        MarionetteDriverManager.getInstance().setup();

        return new MarionetteDriver();
    }

}
