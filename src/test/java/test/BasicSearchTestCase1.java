package test;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import page_objects.MainPageObject;
import retry.RetrySeleniumTestCounter;
import retry.RetrySeleniumTestListener;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;


@Listeners({ RetrySeleniumTestListener.class })
@Features("Общие тесты")
@Stories("Общий поиск по сайту")
public class BasicSearchTestCase1 extends BaseTest {

    // disclaimer: я стараюсь, чтобы в методе под аннотацией @Test остался максимально простой читабельный
    // обычный английский текст - без любых технических подробностей, просто пошаговый сценарий с русскими комментами
    @Title("Проверка базового позитивного сценария")
    @Test(retryAnalyzer = RetrySeleniumTestCounter.class)
    public void basicSearchTest(){
        // вошли на главную страницу сайта неавторизованным пользователем
        openEnvironment();

        // производим поиск с запросом 'получить водительское удостоверение'
        performSearch("получить водительское удостоверение");

        // производим проверки элементов в результатах после осуществления поиска
        checkSearchResultsElements();
    }

    // производим поиск с запросом 'получить водительское удостоверение'
    private void performSearch(String query){
        mainPageObject = new MainPageObject(driver);
        mainPageObject.performSearch(query);
    }

    // производим проверки элементов в результатах после осуществления поиска
    private void checkSearchResultsElements(){
        mainPageObject = new MainPageObject(driver);
        Assert.assertTrue(mainPageObject.checkSearchResultsElements());
    }


}
