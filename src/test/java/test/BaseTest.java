package test;

import core.Singleton;
import helpers.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import page_objects.MainPageObject;
import ru.yandex.qatools.allure.annotations.Step;
import settings.GlobalSettings;

public abstract class BaseTest {

    public WebDriver driver;
    protected String Url;
    protected DriverHelper driverHelper;
    protected Waiters waiters;

    //@BeforeSuite
    public void prepareSuite() {
        String chromeDriver = "chromedriver.exe";
        String chrome = "chrome.exe";

        while (WinProcHelper.isProcessRunning(chromeDriver)) {
            WinProcHelper.killProcess(chromeDriver);
        }

        while (WinProcHelper.isProcessRunning(chrome)) {
            WinProcHelper.killProcess(chrome);
        }

    }

    @BeforeMethod
    public void setup() {

        this.driver = new CreateDriver().createDriver(GlobalSettings.DRIVER);
        this.driverHelper = new DriverHelper(driver);
        this.waiters = new Waiters(driver);
        Singleton.LOGGER.info("Driver was started:" + GlobalSettings.DRIVER);
        this.Url = GlobalSettings.URL;
        Singleton.LOGGER.info("Base url:" + Url);
    }

    //@AfterMethod
    public void clean() {

        if (this.driver != null) {
            this.driver.quit();
            Singleton.LOGGER.info("Driver closed");
        }

    }

    @Step("Открываем сайт неавторизованным пользователем")
    protected void openEnvironment() {
        driver.get(Url);
    }

    public WebDriver getDriver() {
        return driver;
    }

    MainPageObject mainPageObject;
}
