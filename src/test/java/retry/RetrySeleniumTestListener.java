package retry;

import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.IRetryAnalyzer;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.openqa.selenium.OutputType;
import ru.yandex.qatools.allure.annotations.Attachment;
import test.BaseTest;




public class RetrySeleniumTestListener implements ITestListener {

    @Override
    public void onTestFailedButWithinSuccessPercentage(final ITestResult result) {
        /*final IRetryAnalyzer retryAnalyzer = result.getMethod().getRetryAnalyzer();
        if (retryAnalyzer != null && retryAnalyzer instanceof RetrySeleniumTestCounter && ((RetrySeleniumTestCounter) retryAnalyzer).isOnFirstRun()) {
            result.setStatus(ITestResult.SKIP);
        }*/

    }

    @Override
    public void onFinish(ITestContext arg0) {
        System.out.println("----------FINISH ALL----------------\n\n");

    }

    @Override
    public void onStart(ITestContext arg0) {
        System.out.println("----------Start ALL----------------\n\n");

    }


    @Override
    public void onTestFailure(ITestResult result){
        Object currentClass = result.getInstance();
        WebDriver driver = ((test.BaseTest) currentClass).getDriver();
        byte[] srcFile =  ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        saveScreenshot(srcFile);
    }

    @Override
    public void onTestSkipped(ITestResult res) {
        System.out.println(res.getMethod().getMethodName() + " SKIPPED !!! \n\n");

    }

    @Override
    public void onTestStart(ITestResult res) {
        System.out.println("Start test " + res.getMethod().getMethodName());

    }

    @Override
    public void onTestSuccess(ITestResult res) {
        System.out.println(res.getMethod().getMethodName() + " PASSED !!!  \n\n");

    }

    @Attachment(value = "Page screenshot", type = "image/png")
    private byte[] saveScreenshot(byte[] screenshot){
        return screenshot;
    }

}
