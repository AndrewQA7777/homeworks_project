package retry;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetrySeleniumTestCounter implements IRetryAnalyzer {

    private int retryCount;

    @Override
    public boolean retry(final ITestResult result) {
        if (retryCount < 1) {
            retryCount++;
            return false;
        }
        return false;
    }

    public boolean isOnFirstRun() {
        return retryCount == 0;
    }
}
